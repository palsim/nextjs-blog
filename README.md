## next.config.js


```js
const { PHASE_DEVELOPMENT_SERVER } = require('next/constants');

module.exports = (phase) => {
  if (phase === PHASE_DEVELOPMENT_SERVER) {
    return {
      env: {
        mongodb_username: 'dev_username',
        mongodb_password: 'dev_password',
        mongodb_clustername: 'cluster0',
        mongodb_database: 'blogdb-dev',
      },
    };
  }

  return {
    env: {
      mongodb_username: 'prd_username',
      mongodb_password: 'prd_password',
      mongodb_clustername: 'cluster0',
      mongodb_database: 'blogdb',
    },
  };
};
```